package loop_practice;

import java.util.Scanner;

public class DoWhile {

	public static void main(String[] args) {
		
		int num = (int)(Math.random()*100+1);
		System.out.println("要猜的數字是 : "+num);
		int guess;
		Scanner scan = new Scanner(System.in);
		do {
			System.out.println("0~100,猜個數");
			guess = scan.nextInt();
			if(guess > num) {
				System.out.println("太大了");
			}else if(guess < num) {
				System.out.println("太小了");
			}
		}while(guess != num);
			
		System.out.println("Bingo!!!!");
		scan.close();
	}

}
