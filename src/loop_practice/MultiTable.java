package loop_practice;
//99乘法表
public class MultiTable {

	public static void main(String[] args) {
		
		for(int i=1; i<10; i++) {//行
			for(int y=1; y<i+1; y++) {//列
				System.out.print(i+"*"+y+"="+i*y+" ");
				
				//break指跳出一層循環
				/*
				if(y==5) {
					break;
				}
				*/
			}
			System.out.println();
		}

	}

}
